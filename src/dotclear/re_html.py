r"""

Usage::
   >>> print Wiki2XHTML().run(u'''!''Dot''__Clear__ @@markup@@\n\nHello World.''')
   <h5><em>Dot</em><strong>Clear</strong> <tt class="code">markup</tt></h5>

   <p>Hello World.</p>

"""
__serial__ = 2023, 9, 12
__all__ = ["Wiki2XHTML"]

from re import Match

# stdlib
import re
from html.entities import entitydefs
from itertools import dropwhile, zip_longest
from typing import Literal
from urllib.parse import urlsplit

# local
from .nodes import EMPTY, LEAVE, MazNode
from .re_parser import Translator, p_inline

html_special_chars = {
    "'": "&apos;",
    '"': "&quot;",
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
}
html_entities = {f"&{k};": v for k, v in entitydefs.items() if len(v) == 1}


def cmp(a: str, b: str) -> Literal[-1, 0, 1]:
    """Python2/3 compatibility"""
    try:
        return (a > b) - (a < b)
    except TypeError:
        return 0


# FIXME: unescape chars before the output
# XXX: should we use :func:`urllib.quote` on href/src?
class Wiki2XHTML(Translator):
    """Simple conversion from DotClear wiki2xhtml markup to HTML

    .. Warning::
       The behaviour is quite different from the original DotClear parser and a
       few elements have been left unimplemented.
    """

    # ? first space of each line
    _first_space = re.compile(r"(?:^|(?<=\n))(?: |(?=\n))")

    # ? first '> ' sequence of each line, the space being optional
    _first_gt_space = re.compile(r"(?:^|(?<=\n))>(?: |(?=\n))")

    # ? double or more LF
    _block_separator = re.compile(r"\n\n+(?=\S)")

    # ? separate list prefix (# | *) and list value
    _fragment_list = re.compile(
        r"(?P<type>[*#]+) \s* (?P<value> (?: .|\n+? (?:(?=\n[#*])|$) ) | .+\n$ )",
        re.MULTILINE | re.VERBOSE,
    )

    # ? use to beautify ``<li>\n\s*value`` to ``<li>value``
    _li_trim = re.compile(r"(?<=<li>)\s+|(?<!>)(?=\n)\s+?(?=</li>)")

    # ? non-word
    _non_word = re.compile(r"\W+")

    _footnote = 0

    @staticmethod
    def escape(string: str, entities=False) -> str:
        """Escape special HTML characters

        Replace characters with a special significance in HTML by their
        HTML entity equivalent.
        If the optional argument `entities` is set, will use an
        entities' table built from :var:`htmlentitydefs.entitydefs`.
        """
        tr = html_entities if entities else html_special_chars
        return "".join(c in tr and tr[c] or c for c in string)

    ##### blocks
    # noinspection PyUnusedLocal
    @staticmethod
    def b_hr(match: Match) -> str:
        return "<hr />\n"

    def b_p(self, match: Match) -> str:
        return f'<p>{p_inline.sub(self.inlines, match.group("p")).strip()}</p>\n'

    def b_xmp(self, match) -> str:
        return f'<pre class="xmp">{self.escape(match.group("xmp")).strip()}</pre>\n'

    def b_pre(self, match: Match) -> str:
        return (
            "<pre>%s</pre>\n"
            % p_inline.sub(self.inlines, self._first_space.sub("", match.group(match.lastgroup))).rstrip()
        )

    def b_special(self, match: Match) -> str:
        assert match.group("macro") == "html"
        return '<div class="macro %s">%s</div>\n' % (
            self.escape(match.group("macro")),
            match.group("special").strip(),
        )

    def b_head(self, match: Match) -> str:
        return "<h%(n)u>%(value)s</h%(n)u>\n" % {
            "n": 6 - len(match.group("head_level")),
            "value": p_inline.sub(self.inlines, match.group("head_value")),
        }

    def b_blockquote(self, match: Match) -> str:
        return (
            "<blockquote><p>%s</p></blockquote>\n"
            % "</p>\n<p>".join(
                self._block_separator.split(
                    p_inline.sub(
                        self.inlines,
                        self._first_gt_space.sub("", match.group(match.lastgroup)),
                    )
                )
            ).rstrip()
        )

    def b_list(self, match: Match) -> str:
        ltprev = ""
        # ? TODO: i'd like to do it without the nodes tree
        root = node = MazNode("div")
        for m in self._fragment_list.finditer(match.group()):
            ltcurr, value = m.groups()
            for prev, curr in dropwhile(lambda x: not cmp(x[0], x[1]), zip_longest(ltprev, ltcurr)):
                if prev:
                    node = node.parent
                    if node.name == "li":
                        node = node.parent
                if curr:
                    if node.child and node.child.name == "li":
                        node = node.child.prev
                    node += MazNode("%sl" % (curr == "#" and "o" or "u",))
                    node = node.child.prev
            node += MazNode("li") + MazNode(value=p_inline.sub(self.inlines, value))
            ltprev = ltcurr
        # FIXME: there is a bug in MazNode when descending from a higher level than the root
        root.child.parent = root.child
        return self._li_trim.sub("", node2html(root.child))

    # noinspection PyUnusedLocal
    @staticmethod
    def b_nl(match: Match) -> Literal[""]:
        return ""

    ##### inlines
    def i_code(self, match: Match) -> str:
        return f'<tt class="code">{p_inline.sub(self.inlines, match.group(match.lastgroup))}</tt>'

    def i_em(self, match: Match) -> str:
        return f"<em>{p_inline.sub(self.inlines, match.group(match.lastgroup))}</em>"

    def i_strong(self, match: Match) -> str:
        return f"<strong>{p_inline.sub(self.inlines, match.group(match.lastgroup))}</strong>"

    def i_del(self, match: Match) -> str:
        return f"<del>{p_inline.sub(self.inlines, match.group(match.lastgroup))}</del>"

    def i_ins(self, match: Match) -> str:
        return f"<ins>{p_inline.sub(self.inlines, match.group(match.lastgroup))}</ins>"

    def i_footnote(self, match: Match) -> str:
        # FIXME: this does not really work…
        self._footnote += 1
        return (
            f'<sup><a href="#wiki-footnote-{self._footnote}">{self._footnote}</a></sup>'
            '<div class="footnotes">\n <ol>\n  '
            f'<li id="wiki-footnote-{self._footnote}">{match.group("footnote")}</li>\n </ol>\n</div>\n'
        )

    # noinspection PyUnusedLocal
    @staticmethod
    def i_br(match: Match) -> str:
        return "<br />"

    def i_anchor(self, match: Match) -> str:
        return f'<a name="{self._non_word.sub("-", match.group("anchor"))}"></a>'

    def i_acronym(self, match: Match) -> str:
        acronym_title = match.group("acronym_title")
        return "<acronym%s>%s</acronym>" % (
            ' title="%s"' % self.escape(acronym_title.strip()) if acronym_title else "",
            p_inline.sub(self.inlines, match.group("acronym_value")).strip(),
        )

    def i_a(self, match: Match) -> str:
        href = urlsplit(match.group("a_href"))
        link = [f'<a href="{match.group("a_href")}"']
        if match.group("a_title"):
            link.append(f' title="{self.escape(match.group("a_title"))}"')
        if match.group("a_lang"):
            link.append(f' hreflang="{self.escape(match.group("a_lang"))}"')
        if href.scheme:
            # TODO: make a handle for the external using the hostname
            link.append(' class="external"')
        link.append(
            ">%s</a>"
            % (
                p_inline.sub(self.inlines, match.group("a_value"))
                if match.group("a_value")
                else self.escape(match.group("a_href"))
            )
        )
        return "".join(link)

    def i_uri(self, match: Match) -> str:
        return (
            f'<a href="{match.group(match.lastgroup)}" class="external">'
            f"{self.escape(match.group(match.lastgroup))}</a>"
        )

    def i_img(self, match: Match) -> str:
        link = [f'<img src="{match.group("img_src")}"']
        if match.group("img_alt"):
            link.append(f'alt="{self.escape(match.group("img_alt"))}"')
        if match.group("img_desc"):
            link.append(f'longdesc="{self.escape(match.group("img_desc"))}"')
        if match.group("img_align"):
            align = match.group("img_align").strip().lower()[0]
            if align in "lg":
                # ? align left
                link.append('style="float:left; margin: 0 1em 1em 0;"')
            elif align in "cm":
                # ? align center
                link.append('style="display:block; margin:0 auto;"')
            elif align in "rd":
                # ? align right
                link.append('style="float:right; margin: 0 0 1em 1em;"')
            else:
                self.warn(match, f'unknown alignment {match.group("img_align")}')
        link.append("/>")
        return " ".join(link)

    def i_cite(self, match: Match) -> str:
        r = ["<q"]
        if match.group("cite_lang"):
            r.append(f' lang="{self.escape(match.group("cite_lang"))}"')
        if match.group("cite_cite"):
            # FIXME? use urlencode, not escape
            r.append(f' cite="{self.escape(match.group("cite_cite"))}"')
        r.append(f'>{p_inline.sub(self.inlines, match.group("cite_value")).strip()}</q>')
        return "".join(r)


def node2html(node: MazNode) -> str:
    """Return a string representation of the node tree

    .. Warning::
       This is a really specific minimalist implementation.
    """
    data = []
    rs = " "
    for status, node, depth in node.descend(node):
        if status == EMPTY:
            assert not node.name, f"unexpected node name: {node.name!r}"
            data += [f'{rs*depth}{node["value"]}']
        else:
            data += [f'{rs*depth}<{"/"*(status == LEAVE)}{node.name}>']
    return "\n".join(data)
